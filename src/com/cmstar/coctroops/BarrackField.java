package com.cmstar.coctroops;

import java.net.URL;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import com.cmstar.coctroops.ClashData.Barrack;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class BarrackField extends JPanel {

	/**
	 * Create the panel.
	 */
	public BarrackField() {
			setSize(480, 150);
			setLayout(null);
			
			final JLabel lblNewLabel = new JLabel();
			lblNewLabel.setBounds(10, 21, 68, 68);
			fitImage(BarrackField.class.getResource("/com/cmstar/cocres/barrack1.png"), lblNewLabel);
			add(lblNewLabel);
			
			JLabel label = new JLabel();
			label.setBounds(88, 11, 37, 36);
			fitImage(BarrackField.class.getResource("/com/cmstar/cocres/barbarian.png"), label);
			add(label);
			
			JLabel label_1 = new JLabel();
			label_1.setBounds(99, 11, 37, 36);
			add(label_1);
			
			JLabel label_2 = new JLabel();
			label_2.setBounds(146, 11, 37, 36);
			add(label_2);
			
			JLabel label_3 = new JLabel();
			label_3.setBounds(193, 11, 37, 36);
			add(label_3);
			
			JLabel label_4 = new JLabel();
			label_4.setBounds(240, 11, 37, 36);
			add(label_4);
			
			JLabel label_7 = new JLabel();
			label_7.setBounds(417, 11, 37, 36);
			add(label_7);
			
			DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
			for (Barrack barrack : Barrack.values())
				model.addElement(barrack.name);
			final JComboBox<String> comboBox = new JComboBox<String>(model);
			comboBox.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					fitImage(CTWindow.class.getResource("/com/cmstar/cocres/barrack" + comboBox.getSelectedIndex() + ".png"), lblNewLabel);
				}
			});
			comboBox.setBounds(10, 100, 68, 20);
			comboBox.setMaximumRowCount(11);
			add(comboBox);
			
			JSeparator separator = new JSeparator();
			separator.setOrientation(SwingConstants.VERTICAL);
			separator.setBounds(88, 11, 1, 128);
			add(separator);
	}
	
	private void fitImage(URL url, JLabel jLabel) {
		jLabel.setIcon(new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(jLabel.getWidth(), jLabel.getHeight(), java.awt.Image.SCALE_SMOOTH)));
	}
}

package com.cmstar.coctroops;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.cmstar.coctroops.ClashData.Troop;
import com.cmstar.coctroops.ClashData.*;

public class TroopList
{
	public void markDelete(Object o) { o = null; }
	
	// Dynamically set according to ClashData
	private int[] tLevel = new int[Troop.values().length];
	private int[] tQueue = new int[Troop.values().length];
	private List<Barrack> bLevel = new ArrayList<Barrack>();
	private List<DarkBarrack> dBLevel = new ArrayList<DarkBarrack>();
	private List<Troop> elixerTroopOrder = Troop.getOrder(Resource.ELIXER);
	private List<Troop> darkElixerTroopOrder = Troop.getOrder(Resource.DARK_ELIXER);
	private String compatibilityError = "Troop queue is compatibile with available barracks and housing space.";
	/**
	 * Stores troop quantities, time, and space used for each barrack.
	 * <br><b>barrackQueue[0-3][0-9]</b> - Troop quantities of barrack with IDs respectively.
	 * <br><b>barrackQueue[4-5][0-9]</b> - Troop quantities of dark barrack with IDs respectively.
	 * <br><b>barrackQueue[0-3][10]</b> - Time of barrack in seconds.
	 * <br><b>barrackQueue[4-5][10]</b> - Time of dark barrack in seconds.
	 * <br><b>barrackQueue[0-3][11]</b> - Space used of barrack.
	 * <br><b>barrackQueue[4-5][11]</b> - Spaced used of dark barrack.
	 */
	private int[][] barrackQueue = new int[6][12];
	
	public TroopList()
	{
		Arrays.fill(tLevel, 1); // Minimum troop level is 1
		bLevel.add(Barrack.L10);
		bLevel.add(Barrack.L10);
		bLevel.add(Barrack.L0);
		bLevel.add(Barrack.L0);
	}
	
	// Getters and setters for troop levels and queues and barrack levels for external use ONLY
	public int getTLevel(Troop troop) { return tLevel[troop.id]; }
	public void setTLevel(Troop troop, int level) { tLevel[troop.id] = level; }
	public int getTQueue(Troop troop) { return tQueue[troop.id]; }
	public void setTQueue(Troop troop, int queue) { tQueue[troop.id] = queue; }
	public void clearTQueue() { Arrays.fill(tQueue, 0); }
	public Barrack getBLevel(int id) { return bLevel.get(id); }
	public void setBLevel(int id, Barrack barrack) { bLevel.set(id, barrack); }
	public DarkBarrack getDBLevel(int id) { return dBLevel.get(id); }
	public void setDBLevel(int id, DarkBarrack darkBarrack) { dBLevel.set(id, darkBarrack); }
	public String getCompatabilityError() { return compatibilityError; }
	
	// Space in army camp
	// Required barracks level and space
	// Space for queue in barracks
	public boolean checkCompatibility() { 
		int sumHousing = 0;
		int[] barrackHousing = new int[Troop.values().length];
		for (Troop troop : Troop.values())
			sumHousing += troop.space * tQueue[troop.id];
		for (Barrack barrack : bLevel) {
			if (barrack.level != 0)
				barrackHousing[barrack.level - 1] += barrack.cap;
		}
		for (int i = 9; i > 0; i--)
			barrackHousing[i - 1] += barrackHousing[i];
		List<Troop> listArray = Arrays.asList(Troop.values());
		Collections.reverse(listArray);
		for (Troop troop : listArray)
			if (tQueue[troop.id] > 0)
				for (int i = troop.id; i >= 0; i--) {
					barrackHousing[i] -= troop.space * tQueue[troop.id];
					if (barrackHousing[i] < 0) {
						compatibilityError = "Barracks are not sufficiently upgraded to ";
						return false;
					}
				}
		return true;
	}
	
	/**
	* Distributes the troop among all barracks using the linear method and sets values of bQueue.
	*/
	public void distributeLinear()
	{
		System.out.println("Compatibility: " + checkCompatibility());
		int[] idealBarrackTime = new int[2];
		for (Troop troop : Troop.values())
				idealBarrackTime[troop.resource == Resource.ELIXER ? 0 : 1] += tQueue[troop.id] * troop.trainTime;
		int[] tQueue2 = tQueue.clone();
		int transfer;
		for (int barrackID = 0; barrackID < 4; barrackID++)
			for (Troop troop : elixerTroopOrder)
				if (troop.id < bLevel.get(barrackID).level && tQueue2[troop.id] > 0) {
					transfer = Collections.min(Arrays.asList(tQueue2[troop.id], (getBTime(Resource.ELIXER) - barrackQueue[barrackID][10]) / troop.trainTime, bLevel.get(barrackID).cap - barrackQueue[barrackID][11] / troop.space));
					tQueue2[troop.id] -= transfer;
					barrackQueue[barrackID][troop.id] += transfer;
					barrackQueue[barrackID][10] += transfer * troop.trainTime;
					barrackQueue[barrackID][11] += transfer * troop.space;
				}
		// Create and allocate space in list for each barrack
		List<Integer> timeRemaining = new ArrayList<Integer>();
		for (int i = 0; i < barrackQueue.length; i++)
			timeRemaining.add(0);
		// Clean up algorithm for left over troops.
		for (Troop troop : elixerTroopOrder) {
			while (tQueue2[troop.id] > 0) {
				for (int i = 0; i < 4; i++)
					timeRemaining.set(i, troop.id < bLevel.get(i).level && bLevel.get(i).cap - barrackQueue[i][11] >= troop.space ? barrackQueue[i][10] : 9000);
				int barrackID = timeRemaining.indexOf(Collections.min(timeRemaining.subList(0, 4)));
				if (timeRemaining.get(barrackID) == 9000) break; // Break if no suitable place for troop to go. Implemented for error checking. May be deleted in the future.
				tQueue2[troop.id]--;
				barrackQueue[barrackID][troop.id]++;
				barrackQueue[barrackID][10] += troop.trainTime;
				barrackQueue[barrackID][11] += troop.space;
			}
		}
		// --------------------------
		int t1 = 0, t2 = 0;
		for (int i = 0; i < 4; i++) {
			for (int t  = 0; t < 10; t++) {
				t1 += barrackQueue[i][t];
				System.out.print(barrackQueue[i][t] + " ");
			}
			System.out.println(String.format( "%d:%02d", barrackQueue[i][10] / 60, barrackQueue[i][10] % 60) + " " + barrackQueue[i][11] + "/" + bLevel.get(i).cap);
		}
		for (int t : tQueue)
			t2 += t;
		System.out.println(t1 + " troops/" + t2 + " troops | All Included: " + (t1 == t2));
		// --------------------------
		markDelete(tQueue2); markDelete(timeRemaining); System.gc(); // Object clean up
	}
	
	// Distribute troops among barracks using division method
	
	/**
	 * Gets the cost in elixer and dark elixer of the troop queue.
	 * @return cost[0] - Cost in elixer<br>cost[1] - Cost in dark elixer
	 */
	public int[] getCost()
	{
		int[] cost = new int[2];
		for (Troop troop : Troop.values())
			cost[troop.resource.id] += tQueue[troop.id] * Troop.getData(troop, tLevel[troop.id], TroopData.COST) ;
		return cost;
	}
	
	/**
	 * Gets the total time to train all troops.
	 * @param resource - Resource of counted troops.
	 * @return qTime in seconds
	 */
	public int getQTime(Resource resource)
	{
		int qTime = 0;
		for (Troop troop : Troop.values())
			if (troop.resource == resource)
				qTime += tQueue[troop.id] * troop.trainTime;
		return qTime;
	}
	
	/**
	 * Gets the ideal time per barrack.
	 * @return barrackTime in seconds
	 */
	public int getBTime(Resource resource)
	{
		return getQTime(resource) / (4 - Collections.frequency(bLevel, Barrack.L0));
	}
	
}

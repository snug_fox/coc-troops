package com.cmstar.coctroops;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClashData {

	private static Comparator<Troop> sortByTime = new Comparator<Troop>() { @Override public int compare(Troop a, Troop b) { return a.trainTime < b.trainTime ? 1 : a.trainTime > b.trainTime ? -1 : 0; } };
	
	public enum Barrack {
		L0(0), L1(1), L2(2), L3(3), L4(4), L5(5), L6(6), L7(7), L8(8), L9(9), L10(10);
		public int level, cap;
		public String name;
	
		private Barrack(int level) {
			this.level = level;
			this.cap = level == 10 ? 75 : level == 0 ? 0 : 15 + 5 * level;
			this.name = level > 0 ? "Level " + level : "Not Built";
		}
	}
	
	public enum DarkBarrack {
		L0(0), L1(1), L2(2), L3(3), L4(4), L5(5);
		public int level, cap;
		public String name;
		
		private DarkBarrack(int level) {
			this.level = level;
			this.cap = 20 + 10 * level;
			this.name = level > 0 ? "Level " + level : "Not Built";
		}
	}
	
	public enum Troop {
		BARBARIAN(0, 1, 20, new int[][] {{8, 45, 25}, {11, 54, 40}, {14, 65, 60}, {18, 78, 80}, {23, 95, 100}, {26, 110, 150}}, Resource.ELIXER, 6),
			ARCHER(1, 1, 25, new int[][] {{7, 20, 50}, {9, 23, 80}, {12, 28, 120}, {16, 33, 160}, {20, 40, 200}, {22, 44, 300}}, Resource.ELIXER, 6),
			GOBLIN(2, 1, 30, new int[][] {{11, 25, 25}, {14, 30, 40}, {19, 36, 60}, {24, 43, 80}, {32, 52, 100}}, Resource.ELIXER, 6),
			GIANT(3, 5, 120, new int[][] {{11, 300, 500}, {14, 360, 1000}, {19, 430, 1500}, {24, 520, 2000}, {31, 670, 2500}, {40, 880, 3000}}, Resource.ELIXER, 6),
			WALL_BREAKER(4, 2, 120, new int[][] {{12, 20, 1000}, {16, 24, 1500}, {24, 29, 2000}, {32, 35, 2500}, {46, 42, 3000}, {60, 54, 3500}}, Resource.ELIXER, 6),
			BALLOON(5, 5, 480, new int[][] {{25, 150, 2000}, {32, 180, 2500}, {48, 216, 3000}, {72, 280, 3500}, {108, 390, 4000}, {162, 545, 4500}}, Resource.ELIXER, 6),
			WIZARD(6, 4, 480, new int[][] {{50, 75, 1500}, {70, 90, 2000}, {90, 108, 2500}, {125, 130, 3000}, {170, 156, 3500}, {180, 164, 4000}}, Resource.ELIXER, 6),
			HEALER(7, 14, 900, new int[][] {{35, 500, 5000}, {42, 600, 6000}, {55, 840, 8000}, {71, 1176, 10000}}, Resource.ELIXER, 4),
			DRAGON(8, 20, 1800, new int[][] {{140, 1900, 25000}, {160, 2100, 30000}, {180, 2300, 36000}, {190, 2400, 42000}}, Resource.ELIXER, 4),
			PEKKA(9, 25, 2700, new int[][] {{240, 2800, 30000}, {270, 3100, 35000}, {300, 3400, 42000}, {330, 3700, 50000}}, Resource.ELIXER, 4),
			MINION(10, 2, 45, new int[][] {{35, 55, 6}, {38, 60, 7}, {42, 66, 8}, {46, 72, 9}, {50, 78, 10}}, Resource.DARK_ELIXER, 5),
			MR_T(11, 5, 120, new int[][] {{60, 285, 30}, {70, 328, 35}, {80, 380, 40}, {92, 437, 45}, {105, 500, 50}}, Resource.DARK_ELIXER, 5),
			VALKYRIE(12, 8, 900, new int[][] {{88, 750, 70}, {99, 825, 100}, {111, 910, 130}, {124, 1000, 160}}, Resource.DARK_ELIXER, 4),
			GOLEM(13, 30, 2700, new int[][] {{38, 4500, 450}, {42, 5000, 525}, {46, 5500, 600}, {50, 6000, 675}, {54, 6300, 750}}, Resource.DARK_ELIXER, 4),
			WITCH(14, 12, 1200, new int[][] {{25, 75, 250, 6}, {30, 100, 350, 8}}, Resource.DARK_ELIXER, 2);
		public int id, space, trainTime, maxLevel; 
		private int[][] lvlData;
		public Resource resource;
		
		private Troop(int id, int space, int trainTime, int[][] lvlData, Resource resource, int maxLevel) {
			this.id = id;
			this.space = space;
			this.trainTime = trainTime;
			this.lvlData = lvlData;
			this.resource = resource;
			this.maxLevel = maxLevel;
		}
		
		public static int getData(Troop troop, int level, TroopData troopData) {
			return troop.lvlData[level - 1][troopData.id];
		}
		
		public static List<Troop> getOrder(Resource resource) {
			List<Troop> tOrder= new ArrayList<Troop>();
			for (Troop troop : Troop.values())
				if (troop.resource == resource)
					tOrder.add(troop);
			Collections.sort(tOrder, sortByTime);
			return tOrder;
		}
	}
	
	public enum TroopData
	{
		DPS(0), HP(1), COST(2);
		public int id;
		
		private TroopData(int id) { this.id = id; }
	}
	
	public enum Resource {
		ELIXER(0), DARK_ELIXER(1);
		public int id;
		
		private Resource(int id) { this.id = id; }
	}

}

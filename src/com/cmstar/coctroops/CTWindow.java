package com.cmstar.coctroops;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.cmstar.coctroops.ClashData.Barrack;
import com.cmstar.coctroops.ClashData.DarkBarrack;
import com.cmstar.coctroops.ClashData.Troop;

import javax.swing.JPanel;

import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.Color;

import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JSpinner;
import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import javax.swing.ScrollPaneConstants;

public class CTWindow {

	private JFrame ctframe;
	private JPanel mainPanel;
	private TroopList troopList;
	private JComboBox<String> comboBarrack0, comboBarrack1, comboBarrack2, comboBarrack3, comboDarkBarrack0, comboDarkBarrack1;
	private JSpinner spinnerCampSize;
	private static List<String> troopLevels = new ArrayList<String>();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CTWindow ctwindow = new CTWindow();
					ctwindow.ctframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public CTWindow() {
		troopList = new TroopList();
		troopList.setTQueue(Troop.BARBARIAN, 100);
		troopList.setTQueue(Troop.ARCHER, 100);
		troopList.setTQueue(Troop.WALL_BREAKER, 0);
		troopList.distributeLinear();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		String[] bList = new String[11], dbList = new String[6];
		for (Barrack barrack : Barrack.values()) {
			bList[barrack.level] = barrack.name;
		}
		for (DarkBarrack darkbarrack : DarkBarrack.values()) {
			dbList[darkbarrack.level] = darkbarrack.name;
		}
		
		ctframe = new JFrame();
		ctframe.setResizable(false);
		ctframe.setTitle("Clash of Clans Troops");
		ctframe.setSize(972, 424);
		ctframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ctframe.getContentPane().setLayout(null);
		mainPanel = new JPanel();
		mainPanel.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBounds(10, 11, 480, 110);
		ctframe.getContentPane().add(mainPanel);
		
		String[] barrackLevels = new String[11], darkBarrackLevels = new String[DarkBarrack.values().length];
		for (Barrack barrack : Barrack.values())
			barrackLevels[barrack.level] = barrack.name;
		for (DarkBarrack darkBarrack : DarkBarrack.values())
			darkBarrackLevels[darkBarrack.level] = darkBarrack.name;
		for (int i = 0; i < 6; i++)
			troopLevels.add("Level " + (i + 1));
		
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 36, 36, 36, 36, 36, 36, 36, 20, 0};
		gbl_panel.rowHeights = new int[]{21, 14, 20, 14, 20, 21};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		mainPanel.setLayout(gbl_panel);
		
		JLabel lblBarracks = new JLabel("Barracks");
		GridBagConstraints gbc_lblBarracks = new GridBagConstraints();
		gbc_lblBarracks.anchor = GridBagConstraints.SOUTH;
		gbc_lblBarracks.insets = new Insets(0, 0, 5, 5);
		gbc_lblBarracks.gridwidth = 2;
		gbc_lblBarracks.gridx = 4;
		gbc_lblBarracks.gridy = 1;
		mainPanel.add(lblBarracks, gbc_lblBarracks);
		
		comboBarrack0 = new JComboBox<String>(new DefaultComboBoxModel<String>(barrackLevels));
		comboBarrack0.setSelectedIndex(10);
		comboBarrack0.setMaximumRowCount(11);
		GridBagConstraints gbc_comboBarrack0 = new GridBagConstraints();
		gbc_comboBarrack0.gridwidth = 2;
		gbc_comboBarrack0.anchor = GridBagConstraints.NORTH;
		gbc_comboBarrack0.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBarrack0.insets = new Insets(0, 0, 5, 5);
		gbc_comboBarrack0.gridx = 1;
		gbc_comboBarrack0.gridy = 2;
		mainPanel.add(comboBarrack0, gbc_comboBarrack0);
		
		comboBarrack1 = new JComboBox<String>(new DefaultComboBoxModel<String>(barrackLevels));
		comboBarrack1.setSelectedIndex(10);
		comboBarrack1.setMaximumRowCount(11);
		GridBagConstraints gbc_comboBarrack1 = new GridBagConstraints();
		gbc_comboBarrack1.gridwidth = 2;
		gbc_comboBarrack1.anchor = GridBagConstraints.NORTH;
		gbc_comboBarrack1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBarrack1.insets = new Insets(0, 0, 5, 5);
		gbc_comboBarrack1.gridx = 3;
		gbc_comboBarrack1.gridy = 2;
		mainPanel.add(comboBarrack1, gbc_comboBarrack1);
		
		comboBarrack2 = new JComboBox<String>(new DefaultComboBoxModel<String>(barrackLevels));
		comboBarrack2.setSelectedIndex(10);
		comboBarrack2.setMaximumRowCount(11);
		GridBagConstraints gbc_comboBarrack2 = new GridBagConstraints();
		gbc_comboBarrack2.gridwidth = 2;
		gbc_comboBarrack2.anchor = GridBagConstraints.NORTH;
		gbc_comboBarrack2.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBarrack2.insets = new Insets(0, 0, 5, 5);
		gbc_comboBarrack2.gridx = 5;
		gbc_comboBarrack2.gridy = 2;
		mainPanel.add(comboBarrack2, gbc_comboBarrack2);
		
		comboBarrack3 = new JComboBox<String>(new DefaultComboBoxModel<String>(barrackLevels));
		comboBarrack3.setSelectedIndex(10);
		comboBarrack3.setMaximumRowCount(11);
		GridBagConstraints gbc_comboBarrack3 = new GridBagConstraints();
		gbc_comboBarrack3.gridwidth = 2;
		gbc_comboBarrack3.anchor = GridBagConstraints.NORTH;
		gbc_comboBarrack3.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBarrack3.insets = new Insets(0, 0, 5, 5);
		gbc_comboBarrack3.gridx = 7;
		gbc_comboBarrack3.gridy = 2;
		mainPanel.add(comboBarrack3, gbc_comboBarrack3);
		
		JLabel lblDarkBarracks = new JLabel("Dark Barracks");
		GridBagConstraints gbc_lblDarkBarracks = new GridBagConstraints();
		gbc_lblDarkBarracks.anchor = GridBagConstraints.SOUTH;
		gbc_lblDarkBarracks.insets = new Insets(0, 0, 5, 5);
		gbc_lblDarkBarracks.gridwidth = 2;
		gbc_lblDarkBarracks.gridx = 3;
		gbc_lblDarkBarracks.gridy = 3;
		mainPanel.add(lblDarkBarracks, gbc_lblDarkBarracks);
		
		JLabel lblCampSize = new JLabel("Camp Size");
		GridBagConstraints gbc_lblCampSize = new GridBagConstraints();
		gbc_lblCampSize.anchor = GridBagConstraints.SOUTH;
		gbc_lblCampSize.gridwidth = 2;
		gbc_lblCampSize.insets = new Insets(0, 0, 5, 5);
		gbc_lblCampSize.gridx = 6;
		gbc_lblCampSize.gridy = 3;
		mainPanel.add(lblCampSize, gbc_lblCampSize);
		
		comboDarkBarrack0 = new JComboBox<String>(new DefaultComboBoxModel<String>(darkBarrackLevels));
		comboDarkBarrack0.setSelectedIndex(DarkBarrack.values().length - 1);
		comboDarkBarrack0.setMaximumRowCount(11);
		GridBagConstraints gbc_comboDarkBarrack0 = new GridBagConstraints();
		gbc_comboDarkBarrack0.gridwidth = 2;
		gbc_comboDarkBarrack0.anchor = GridBagConstraints.NORTH;
		gbc_comboDarkBarrack0.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboDarkBarrack0.insets = new Insets(0, 0, 5, 5);
		gbc_comboDarkBarrack0.gridx = 2;
		gbc_comboDarkBarrack0.gridy = 4;
		mainPanel.add(comboDarkBarrack0, gbc_comboDarkBarrack0);
		
		comboDarkBarrack1 = new JComboBox<String>(new DefaultComboBoxModel<String>(darkBarrackLevels));
		comboDarkBarrack1.setSelectedIndex(DarkBarrack.values().length - 1);
		comboDarkBarrack1.setMaximumRowCount(11);
		GridBagConstraints gbc_comboDarkBarrack1 = new GridBagConstraints();
		gbc_comboDarkBarrack1.gridwidth = 2;
		gbc_comboDarkBarrack1.anchor = GridBagConstraints.NORTH;
		gbc_comboDarkBarrack1.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboDarkBarrack1.insets = new Insets(0, 0, 5, 5);
		gbc_comboDarkBarrack1.gridx = 4;
		gbc_comboDarkBarrack1.gridy = 4;
		mainPanel.add(comboDarkBarrack1, gbc_comboDarkBarrack1);
		
		spinnerCampSize = new JSpinner();
		spinnerCampSize.setModel(new SpinnerNumberModel(240, 20, 240, 5));
		spinnerCampSize.setBorder(UIManager.getBorder("Spinner.border"));
		GridBagConstraints gbc_spinnerCampSize = new GridBagConstraints();
		gbc_spinnerCampSize.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerCampSize.gridwidth = 2;
		gbc_spinnerCampSize.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCampSize.gridx = 6;
		gbc_spinnerCampSize.gridy = 4;
		mainPanel.add(spinnerCampSize, gbc_spinnerCampSize);
		
		JScrollPane scrollPaneTroopQueue = new JScrollPane();
		scrollPaneTroopQueue.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				((JScrollPane)e.getSource()).getVerticalScrollBar().setValue((int)((JScrollPane)e.getSource()).getVerticalScrollBar().getValue() + (int)(e.getPreciseWheelRotation() * 12));
			}
		});
		scrollPaneTroopQueue.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		scrollPaneTroopQueue.setBounds(10, 132, 480, 256);
		ctframe.getContentPane().add(scrollPaneTroopQueue);
		
		JPanel panelTroopQueue = new JPanel();
		panelTroopQueue.setPreferredSize(new Dimension(scrollPaneTroopQueue.getWidth() - 4, (Troop.values().length + Troop.values().length % 2) / 2 * 69 + 5));
		scrollPaneTroopQueue.setViewportView(panelTroopQueue);
		panelTroopQueue.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		for (Troop troop : Troop.values())
			panelTroopQueue.add(new TroopField(troop));
		
		BarrackField panel = new BarrackField();
		panel.setLocation(500, 11);
		ctframe.getContentPane().add(panel);
	}
}


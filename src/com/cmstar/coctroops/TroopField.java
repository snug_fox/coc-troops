package com.cmstar.coctroops;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.cmstar.coctroops.ClashData.Troop;

public class TroopField extends JPanel {
	
	private static final long serialVersionUID = 1L;

	public TroopField(final Troop troop) {
		// Panel
		setPreferredSize(new Dimension(226, 64));
		setBackground(Color.WHITE);
		setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		setLayout(null);
		// Icon
		JLabel lblIcon = new JLabel(new ImageIcon(CTWindow.class.getResource("/com/cmstar/cocres/" + troop.name().toLowerCase() + ".png")));
		lblIcon.setBounds(4, 4, 56, 56);
		add(lblIcon);
		// Time
		final JLabel lblTime = new JLabel("00:00:00");
		lblTime.setIcon(new ImageIcon(CTWindow.class.getResource("/com/cmstar/cocres/clock.png")));
		lblTime.setBounds(144, 4, 72, 20);
		add(lblTime);
		// Queue Spinner
		JSpinner spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				int sTime = (int)((JSpinner)e.getSource()).getValue() * troop.trainTime;
				lblTime.setText(String.format("%02d:%02d:%02d", sTime / 3600, sTime % 3600 / 60, sTime % 60));
			}
		});
		spinner.setModel(new SpinnerNumberModel(0, 0, 240, 1));
		spinner.setBounds(70, 4, 64, 20);
		add(spinner);
		// Cost
		final JLabel lblCost = new JLabel("0");
		lblCost.setIcon(new ImageIcon(CTWindow.class.getResource("/com/cmstar/cocres/dropElixir.png")));
		lblCost.setBounds(144, 35, 72, 20);
		add(lblCost);
		// Level Combo Box
		List<String> levels = new ArrayList<String>();
		for (int i = 1; i <= troop.maxLevel; i++)
			levels.add("Level" + i);
		JComboBox<String> comboLevel = new JComboBox<String>(new DefaultComboBoxModel<String>(levels.toArray(new String[troop.maxLevel])));
		comboLevel.setBounds(70, 35, 64, 20);
		add(comboLevel);
	}

}

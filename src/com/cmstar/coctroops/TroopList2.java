package com.cmstar.coctroops;

import com.cmstar.coctroops.ClashData.*;

public class TroopList2 {
	
	private int[][] tList = new int[2][15], bList = new int[4][10];
	private int[] barracks = new int[6], tListD;
	private int camps = 20;
	
	public TroopList2() {
		// Set list to defaults
		for (int i = 0; i < tList.length; i++) {
			tList[i][1] = 1;
		}
		barracks[0] = 1;
		setTroop(Troop.ARCHER, 40, false);
		distribute();
	}
	
	public void setTroop(Troop troop, int n, boolean add) {
		tList[0][troop.id] = add ? tList[0][troop.id] + n : n;
	}
	
	public void setLevel(Troop troop, int level) {
		tList[1][troop.id] = level;
	}
	
	public int getLevel(Troop troop) {
		return tList[1][troop.id];
	}
	
	public void distribute() {
		if (getSpace() < camps) {
			return;
		}
		tListD = tList[0];
		for (int barrackId = 0; barrackId < 4; barrackId++) {
			System.out.println(barrackId + ", " + getTime());
			while (getTime(barrackId) <= getTime() / 4 || (barrackId == 3 && getTime(3) > 0)) {
				 for (int id = 9; id >= 0; id--) {
					 if (tListD[id] > 0) {
						 addToBarrack(id, 1, barrackId);
					 }
				 }
			}
		}
		System.out.println(bList.toString());
	}
	
	public void addToBarrack(int id, int n, int barrackId) {
		bList[barrackId][id] += n;
		tListD[id] -= n;
	}
	
	public int getTime() {
		int time = 0;
		for (Troop troop : Troop.values()) {
			time += tList[0][troop.id] * troop.trainTime;
		}
		return time;
	}
	
	public int getTime(int[] list) {
		int time = 0;
		for (Troop troop : Troop.values()) {
			time += list[troop.id] * troop.trainTime;
		}
		return time;
	}
	
	public int getTime(int barrackId) {
		int time = 0;
		for (Troop troop : Troop.values()) {
			if (troop.id < 10) {
				time += bList[barrackId][troop.id] * troop.trainTime;
			}
		}
		return time;
	}
	
	public int getSpace() {
		int space = 0;
		for (Troop troop : Troop.values()) {
			space += tList[0][troop.id] * troop.space;
		}
		return space;
	}
	
	public int getDamage() {
		int damage = 0;
		for (Troop troop : Troop.values()) {
			damage += tList[0][troop.id] * Troop.getData(troop, tList[1][troop.id], TroopData.DPS);
		}
		return damage;
	}
	
	public int getHealth() {
		int health = 0;
		for (Troop troop : Troop.values()) {
			health += tList[0][troop.id] * Troop.getData(troop, tList[1][troop.id], TroopData.HP);
		}
		return health;
	}
	
	public int getCost() {
		int cost = 0;
		for (Troop troop : Troop.values()) {
			cost += tList[0][troop.id] * Troop.getData(troop, tList[1][troop.id], TroopData.COST);
		}
		return cost;
	}
	
}
